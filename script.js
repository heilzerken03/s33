// https://jsonplaceholder.typicode.com/todos

// 3
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	console.log(data)
})

// 4 MAP METHOD (.map)

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
    let list = data.map((todo_item) => {
    	return todo_item.title
    })
    console.log(list)
 })


// 5
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => console.log(data))

// 6
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => {
console.log(data) 
console.log(`The item "${data.title}" on the list has a status of ${data.completed}`)
})

// 7
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New To do list item',
		body: 'Hello, world! This is my new to do list!',
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// 8
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To do list item!',
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// 9
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		Title: 'Number 9',
		Description: 'Changing the data structure',
		Status: 'Completed',
		Date: 'October 03, 2022',
		userId: 2
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// 10

fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Number 10',
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// 11

fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({

		Status: 'Complete',
		Completed: "October 03, 2022"
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// 12

fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'DELETE'
})